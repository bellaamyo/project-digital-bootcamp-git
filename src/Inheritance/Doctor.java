/*
 * Create By : Bella Amy Octaviani
 * Create At : 12 Juni 2023
 */
package Inheritance;

/**
 *
 * @author BELLA
 */
public class Doctor extends Person{
    public String spesialis;
    
    public Doctor(){
        
    }
    public Doctor(String name, String addres, String spesialis){
        super();
        this.spesialis = spesialis;
        
    }
    
    public void periksa(){
        System.out.println("Sya adalah dokter " + spesialis + " yang kompeten. ");
    }
    public void greeting(){
        super.greeting();
      System.out.println("Hello, My Name is : " + name + ". ");
      System.out.println("I, Come from : " + addres + ". ");
      System.out.println("My spesialis is : " + spesialis + "Doctor");
  }
}
