/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inheritance;

/**
 *
 * @author BELLA
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//      Person a = new Programmer("ana", "bandung", "ktp");
//      Person b = new Teacher("bell", "amy", "ayi");
//      Person c = new Teacher("anabel", "bandung", " miii");
      Person d = new Chef("bella", "Cimahi", "Indomie");
      
//      sayHai(a);
//      sayHai(b);
      sayHai(d);
      
//      Teacher b = new Teacher();
//      b.name = "anabul";
//      b.Addres = "Istanbul";
//      b.subject = "Matematika";
//      
//      Doctor c = new Doctor();
//      c.name = " eneng";
//      c.Addres = "cimahi";
//      c.spesialis = "Bedah";
//      
//      Programmer d = new Programmer();
//      d.name = "Anggea";
//      d.Addres = "Soreang";
//      d.Teknologi = "Java";
//      
//      a.greeting();
//      System.out.println();
//      b.greeting();
//      System.out.println();
//      c.greeting();
//      System.out.println();
//      d.greeting();
//      System.out.println();

    }
    static void sayHai(Person person){
        String message;
        if(person instanceof Programmer){
            Programmer programmer = (Programmer) person;
            message = "Hello saya " + programmer.name + ". Seorang programmer " + programmer.teknologi + ". ";
        }else if(person instanceof Teacher){
            Teacher teacher = (Teacher) person;
            message = "Hell0 saya " + teacher.name + ". seorang guru " + teacher.subject + ". ";
        }else if(person instanceof Doctor){
            Doctor dokter = (Doctor) person;
            message = "Hello saya " + dokter.name + ". seorang dokter " + dokter.spesialis + ". ";
        }else if(person instanceof Chef){
            Chef chef = (Chef) person;
            message = "Hello saya " + chef.name + ". seorang juru masak " + chef.memasak + ". ";
        }else if(person instanceof Tailor){
            Tailor tailor = (Tailor) person;
            message = "Hello saya " + tailor.name + ". seorang juru penjahit " + tailor.pakaian + ". ";
        }else{
             message = "Hello saya " + person.name + ". ";
        }
           System.out.println(message);
    }
    
    
}
