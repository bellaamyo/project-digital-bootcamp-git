/*
 * Create By : Bella Amy Octaviani
 * Create At : 12 Juni 2023
 */
package Inheritance;

/**
 *
 * @author BELLA
 */
public class Teacher extends Person{
    public String subject;
    
    public Teacher(){
        
    }
    public Teacher(String name, String addres, String subject){
        super(name, addres);
        this.subject = subject;
    }
  public void mengajar(){
      System.out.println("Saya adalah guru " + subject + " yang baik . ");
  }
  public void greeting(){
      System.out.println("Hello, My Name is " + name + ". ");
      System.out.println("I, Come from " + addres + ". ");
      System.out.println("My job is " + subject + "Teacher");
  }
    
}
