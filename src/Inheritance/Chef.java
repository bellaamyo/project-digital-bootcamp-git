package Inheritance;

public class Chef extends Person{
	public String memasak;
	
	public Chef() {
		
	}
	public Chef(String name, String addres, String memasak){
        super(name, addres);
        this.memasak = memasak;
    }
	public void memasak(){
        System.out.println("Sya dapat memasak apapun termasuk " + memasak + ". ");
    }
    public void greeting(){
        super.greeting();
      System.out.println("Hello, My Name is : " + name + ". ");
      System.out.println("I am come from " + addres + ".");
      System.out.println("I can cook anything including : " + memasak + ".");
  }
}
